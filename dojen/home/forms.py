from django import forms


class SearchForm(forms.Form):
    track_id = forms.CharField(required=True, widget=forms.TextInput(attrs={'class' : 'fwow fadeInLeft', 'placeholder': 'Enter your TrackID', ' data-wow-delay':'0.5s'}))
    
    
    