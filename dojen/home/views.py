from django.shortcuts import render, get_object_or_404
from .models import Item, Update
from .forms import SearchForm
import json
from time import mktime
import datetime
import calendar
# Create your views here.


def index(request):
    return render(request, 'index.html')

def search(request):
    if request.method == 'POST':
        for key, value in request.POST.items():
            print(key, value)
        track_id = request.POST.get('track_id')
        obj = get_object_or_404(Item, track_id=track_id)
        time_sent = obj.timestamp
        time_sent = calendar.timegm(time_sent.timetuple())
        time_sent = datetime.datetime.fromtimestamp(float(time_sent))
        shipping_category = obj.product
        weight = obj.weight
        eta = obj.time
        eta = calendar.timegm(eta.timetuple())
        eta = datetime.datetime.fromtimestamp(float(eta))
        
        obj_2= Update.objects.filter(track_it__track_id =track_id)
        
        
        tmpl_vars = {
        'form': SearchForm(),
        'track_id': track_id,
        
        'weight': weight,
        
        'shipping_category':shipping_category,
        'eta' :eta,
        'time_sent' : time_sent,
        'info':obj_2
            
        }
        print tmpl_vars
        
    else:
        tmpl_vars = {'form':SearchForm()}
        
        
    return render(request, 'search.html', tmpl_vars)