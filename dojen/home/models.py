from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Item(models.Model):
    track_id = models.CharField(max_length = 20)
    timestamp =  models.DateTimeField(auto_now = False)
    time = models.DateTimeField( auto_now=False)
    product = models.CharField( max_length = 20)
    weight = models.CharField(max_length =20 )
    def __str__(self):
        return '%s' % (self.track_id)
    
class Update(models.Model):
    track_it = models.ForeignKey(Item, on_delete=models.CASCADE)
    time = models.DateTimeField( auto_now=False)
    update_text = models.TextField(default = '')
    def __str__(self):
        return '%s - %s' % (self.track_it,self.update_text)